/* GruntFile config */
//var util = require('./libs/grunt/utils.js');

module.exports = function(grunt) {
	// Load the plugins.
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-connect');
	//grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	//grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-livereload');

	//Grunt Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
			all: ['Gruntfile.js']
		},
		less: {
			application: {
				options: {
					paths: ["css"],
					cleancss: true,
					imports: {
						// Use the new "reference" directive, e.g.
						// @import (reference) "variables.less";
						reference: [
						"less/mixins.less", 
						"less/variables.less" 
						]
			      	}
			  	},
				files: [
				{
					expand: true,
					cwd: 'less',
					src: ['main.less', '!{var,mix}*.less'],
					dest: 'css/',
					ext: '.css'
				}
			  ]
			}
		},
		watch: {
			options: {
				livereload: true
			},
			scripts: {
				files: ['js/libs/*.js', 'js/app/**/*.js'],
				tasks: ['jshint'],
				options: {
					spawn: false,
					livereload: true,
					livereloadOnError: false
				},
			},
			styles: {
				files: ['**/*.less'],
				tasks: ['less:application'],
				options: {
					livereload: true,
					livereloadOnError: false
				}
			},
			templates: {
				files: ['*.html'],
				options: {
					livereload: true,
					livereloadOnError: false
				}
			}

		},
		copy: {
			bootstrap :{
				files: [
					{
						expand: true, 
						flatten: true, 
						src: ['libs/bootstrap/dist/js/**'], 
						dest: 'js/libs/', 
						filter: 'isFile'
					},
					{
						expand: true, 
						flatten: true,
						src: [
							'libs/bootstrap/dist/css/**',
							'libs/ui-select/dist/*.css'
						],
						dest:  'css/', 
						filter: 'isFile'
					},
					{
						expand: true, 
						flatten: true,
						src: ['libs/bootstrap/dist/fonts/**'],
						dest:  'fonts/',
						filter: 'isFile'
					}
				]
			},
			angular: {
				files: [
					{
						expand: true, 
						flatten: true, 
						src: [
							'libs/angular/angular.min.js', 
							'libs/angular/angular.min.js.map',
							'libs/angular-*/angular-*.min.js',
							'libs/angular-*/angular-*.min.js.map', 
							'libs/angular-*/angular-*.js',
							'libs/angular-ui-router/release/angular-*.js', 
							'libs/angular-*/angular-*.min.js.map',
							'libs/angular-bootstrap/ui-bootstrap.min.js',
							'libs/angular-bootstrap/ui-bootstrap-tpls.min.js',
							'libs/spin.js/spin.js',
							'libs/ng-loaders/*.js',
							'libs/ui-select/dist/select.min.js'
						], 
						dest: 'js/libs/', 
						filter: 'isFile'
					}
				]
			},
			jquery: {
				files: [
					{
						expand: true, 
						flatten: true,
						src: ['libs/jquery/dist/**'],
						dest:  'js/libs/',
						filter: 'isFile'
					}
				]
			}
		},
		connect: {
			demo: {
				options: {
					port: 8000,
					protocol: 'http',
					hostname: '127.0.0.1',
					base: '.',
					keepalive: true,
					middleware: function(connect, options) {

						var base = Array.isArray(options.base) ? options.base[options.base.length - 1] : options.base;
						
						return [
							//Enable CORS
							connect().use(function (req, res, next) {
							 	res.setHeader('Access-Control-Allow-Credentials', true);
							 	res.setHeader("Access-Control-Allow-Headers", "*");
							 	res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
							 	res.setHeader('Access-Control-Allow-Origin', '*');
							 	next();
							}),
							
							connect.static(base),
							connect.directory(base)
			              ];
		          	},
		          	livereload: true
		      	}
		  	}
		}
	});


	//Grunt Tasks definition
	grunt.registerTask('default', ['prepare', 'watch']);
	grunt.registerTask('init-js', ['copy:angular', 'copy:bootstrap', 'copy:jquery']);
	grunt.registerTask('init-css', ['less:application']);
	grunt.registerTask('prepare', ['init-js', 'init-css']);
	grunt.registerTask('demo', ['prepare', 'connect:demo']);
};