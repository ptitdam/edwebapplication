edApplication.controller('ProductsCtrl', ['$scope', '$state', '$stateParams', 'EDRestService', 'EDHistory', '$location',
	function($scope, $state, $stateParams, EDRestwebservice, EDHistory, $location) {
		$scope.answerId = $stateParams.aid;
		$scope.viewMode = 'carousel';

		//init filter
		$scope.filters = {
			'filter_spiritueux': '',
			'filter_aroma': '',
			'filter_price': ''
		};

		$scope.switchViewList = function() {
			$scope.viewMode = $scope.viewMode == 'carousel' ? 'list' : 'carousel';
		}

		/**
		 * Back button configuration
		 */
		$scope.goBack = function() {
			if ($scope.answerId) {
				EDRestwebservice.getQuestionbyAnswerId($scope.answerId, function(question) {
					$state.go('question', {'qid': question.id});
				});
			} else {
				$state.go('question');
			}
		}

		/**
		 * Page Configuration
		 */
		EDRestwebservice.getProductsByAnswer($scope.answerId).then(function(products) {
			$scope.rawProducts = products;
			$scope.products = products;

			//init form filter values
			$scope.list_spiritueux = [];
			angular.forEach($scope.rawProducts, function(pdt) {
				if (typeof(pdt.spiritueux) == 'string') {
					$scope.list_spiritueux.push(pdt.spiritueux);
				} else {
					angular.forEach(pdt.spiritueux, function(item) {
						$scope.list_spiritueux.push(item);
					});
				}
			});

			$scope.list_aromas = [];
			angular.forEach($scope.rawProducts, function(pdt) {
				if (typeof(pdt.aromes) == 'string') {
					$scope.list_aromas.push(pdt.aromes);
				} else {
					angular.forEach(pdt.aromes, function(item) {
						$scope.list_aromas.push(item);
					})
				}
				
			});

			//Historize
			var current = {
				'question' : {'question' : 'Résultat'}, 
				'answer' : {},
				'url' : '#' + $location.path()
			};
			EDHistory.add(current);

		});
	}
]);
/*edApplication.controller("productsCtrl", ['$scope', '$routeParams', 'EDRestService', '$location', 
	function($scope, $routeParams, EDService, $location) { 
		$scope.aid = angular.isDefined($routeParams.aid) ? parseInt($routeParams.aid) : null;

		if ($scope.aid == null) {
			$location.path('/');
		}

		EDService.getProducts($scope.aid, function(data) {

			$scope.products = data;

			//get spiritueux
			$scope.spiritueux = [];
			angular.forEach($scope.products, function(alcohol) {
				if (angular.isArray(alcohol.spiritueux)) {
					angular.forEach(alcohol.spiritueux, function(spiritueux) {
						if ($scope.spiritueux.indexOf(spiritueux) < 0) {
							$scope.spiritueux.push(spiritueux);
						}
					});
				} else if ($scope.spiritueux.indexOf(alcohol.spiritueux) < 0) {
					$scope.spiritueux.push(alcohol.spiritueux);
				}
			});
			$scope.spiritueux = $scope.spiritueux.sort();
			$scope.curSpiritueux = "";

			//get Aromas
			$scope.aromes = [];
			angular.forEach($scope.products, function(alcohol) {
				if (angular.isArray(alcohol.aromes)) {
					angular.forEach(alcohol.aromes, function(arome) {
						if ($scope.aromes.indexOf(arome) < 0) {
							$scope.aromes.push(arome);
						}
					});
				} else if ($scope.aromes.indexOf(alcohol.aromes) < 0) {
					$scope.aromes.push(alcohol.aromes);
				}
			});

			$scope.aromes = $scope.aromes.sort();
			$scope.curArome = "";

			$scope.prices = [ 
				{'value' : 1, 'content' : '€'},
				{'value' : 2, 'content' : '€€'},
				{'value' : 3, 'content' : '€€€'}
			];
			$scope.curPrice = "";

		});
	}
]);
*/