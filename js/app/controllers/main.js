edApplication.controller('MainCtrl', ['$scope', '$http', '$sce', '$timeout', '$state', 'EDRestService', 'EDHistory',
	function($scope, $http, $sce, $timeout, $state, EDRestwebservice, EDHistory) {

		//reset History
		EDHistory.reset()

		$scope.loaderPct = 0;
		$scope.loading = false;

		//add function on click
		$scope.loadData = function() {
			$scope.loading = true;

			//init data into local database
			$timeout(function () {
				EDRestwebservice.getAllQuestionsAndAnswers(true, function(data) {
					$scope.loaderPct = 50;

					//load Products
					EDRestwebservice.getAllProducts(function(){
						$scope.loaderPct = 100;
						
						$timeout(function(){}, 500);

						//redirect to first question page
						$state.transitionTo('question');

					});
				}, function() {
					// An alert dialog
				});
			}, 3000);
		};
	}
]);