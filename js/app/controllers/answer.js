edApplication.controller('AnswerCtrl', ['$scope', '$stateParams', '$state','EDRestService', '$location', 'EDHistory',
	function($scope, $stateParams, $state, EDRestService, $location, EDHistory) {

		$scope.qid = angular.isDefined($stateParams.qid) ? parseInt($stateParams.qid) : null;
	    $scope.aid = angular.isDefined($stateParams.aid) ? parseInt($stateParams.aid) : null;

	    var updateHistory = function(answer) {

	    	var last = EDHistory.getLast();
	    	if (last) {
	    		current = {
					'question' : last.question, 
					'answer' : angular.isDefined(answer) ? answer : {},
					'url' : last.url
				}
				EDHistory.update(EDHistory.getLength() - 1, current);
	    	}
		};


	    //If there is no more question, we redirect to product list
		if ($scope.aid != null) {
			EDRestService.getAnswer($scope.aid).then(function(currentAnswer) {
				
				//update History with the answer
				updateHistory(currentAnswer);

				//redirect to question
				if (currentAnswer.next_qid == null) {
					//No more question
					$state.transitionTo('products', {'aid' : $scope.aid});
				} else {

					//next question
					$state.go('question', {'qid': currentAnswer.next_qid});
				}
			})
		}
	}
]);