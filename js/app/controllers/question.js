edApplication.controller('QuestionCtrl', ['$scope', '$stateParams', '$state','EDRestService', 'EDHistory', '$location',
	function($scope, $stateParams, $state, EDRestService, EDHistory, $location) {

		$scope.questionId = angular.isDefined($stateParams.qid) ? parseInt($stateParams.qid) : null;

		var saveHistory = function(question) {
			//if rollback, we delete history current page
			EDHistory.removeAfter("#" + $location.path());

			var current = {
				'question' : question, 
				'answer' : {},
				'url' : '#' + $location.path()
			};
			EDHistory.add(current);

		};


		if ($stateParams.qid) {
			EDRestService.getQuestionbyId($scope.questionId, function(currentQuestion) {
				$scope.current_question = currentQuestion;
				saveHistory($scope.current_question);
			});
		} else {
			EDRestService.getRootQuestion(function(currentQuestion) {
				$scope.current_question = currentQuestion;
				saveHistory($scope.current_question);
			});
		}
	}
]);