edApplication.service('EDRestService', ['$http', '$q', function($http, $q) {
	var baseUrl = 'http://demo6546857.mockable.io/';
	var questionTreeUrl = baseUrl + 'questiontree';
	var productsUrl = baseUrl + 'products';


	var _questionanswers = null;
	var _rawQuestions = null;
	var _rawAnswers = null;
	var _products = null;

	this._initData = function(sorted) {
		var deferred = $q.defer();

		this.getAllQuestionsAndAnswers(sorted, function(data) {
			deferred.resolve(data);
		}, function() {
			deferred.reject("question not initialized");
		});

		return deferred.promise;
	}

	/**
	 * Get all questions and answers of application
	 * 
	 */
	this.getAllQuestionsAndAnswers = function(sorted, successCallback, errorCallback) {
		if (typeof(successCallback) == 'function') {
			if (!_questionanswers) {
				console.log("Questions and answers loaded from Webservice GET");
				
				$http(
					{ method: 'GET', url: questionTreeUrl}
				).
				error(function(data, status, headers, config) {
				    if (typeof errorCallback == 'function') {
				    	errorCallback(data);
				    }
				}).
				success(function(result, status, headers, config) {
					_rawQuestions = result.questions;
					_rawAnswers = result.answers;
					var _searchAnswer = function(id) {
						var ret = false;
						angular.forEach(_rawAnswers, function(a) {
							if (parseInt(a.id) == id) {
								ret = a;
							}
						});
						return ret;
					};

					var tempQuestions = _rawQuestions;
					angular.forEach(tempQuestions, function(value, key) {
						angular.forEach(value.answers, function(v, k) {
							tempQuestions[key].answers[k] = _searchAnswer(parseInt(v));
						});
					});

					_questionanswers = tempQuestions;
					successCallback(_questionanswers);
				});
			} else {
				console.log("Questions and answers loaded from Singleton");
				successCallback(_questionanswers);
			}
		} else {
			if (typeof errorCallback == 'function') {
		    	errorCallback(data);
		    } else {
		    	throw Error("successCallback must be mandatory and it must be a function");
		    }
		}
	};

	this.getAnswer = function (id) {
		var deferred = $q.defer();

		this._initData().then( function(data) {
			var ret = false;
			angular.forEach(_rawAnswers, function(a) {
				if (parseInt(a.id) == id) {
					ret = a;
				}
			});
			ret ? deferred.resolve(ret) : deferred.reject("answer("+id+") not found");
		});
		return deferred.promise;
	};

	this.getRootQuestion = function(callback) {
		if (typeof(callback) == 'function') {
			this._initData().then(function(data) {
				var ret = false;
				angular.forEach(_questionanswers, function(q) {
					if (!q.parent) {
						ret = q;
					}
				});
				callback(ret);
			});
		}
	};

	this.getQuestionbyId = function(questionId, callback) {
		if (typeof(callback) == 'function') {
			this._initData().then(function(data) {
				var ret = false;
				angular.forEach(_questionanswers, function(q) {
					if (parseInt(q.id) == questionId) {
						ret = q;
					}
				});
				callback(ret);
			});
		}
	};

	this.getQuestionbyAnswerId = function(answerId, callback) {
		if (answerId > 0 && typeof(callback) == 'function') {
			var singleton = this;

			this._initData().then( function(data) {
				var found = false;

				angular.forEach(_rawQuestions, function(q) {
					if(q.answers.length > 0 && typeof(q.answers[0]) == 'object') {
						angular.forEach(q.answers, function(a) {
							if(a.id == answerId) {
								found = parseInt(q.id)
							}
						})
					} else if (answerId > 0 && q.answers.indexOf(answerId) >= 0) {
						found = parseInt(q.id);
					}
				});

				var exec = function(item) {
					callback(item);
				};
				found ? singleton.getQuestionbyId(found, exec) : singleton.getRootQuestion(exec);
			});
		} else if (!answerId) {
			singleton.getRootQuestion(function(item) {
				callback(item);
			});
		} else {
			throw Error("callback function is mandatory");
		}
	};

	this.getAllProducts = function(success) {
		if (_products == null) {
			$http({
				method: 'GET', 
				url: productsUrl
			}).
			success(function(result, status, headers, config) {
				_products = result;

				if (typeof(success) == 'function') {
					success(_products);
				}
			}).
			error(function(data, status, headers, config) {
				throw Error(data);
			});
		} else if (typeof(success) == 'function') {
			success(_products);
		}
	};
	

	this.getProductsByAnswer = function(answerId) {
		var singleton = this;
		var deferred = $q.defer();

		var products = [];
		this.getAnswer(answerId).then(function(data) {
			if (data && data.products.length > 0) {
				angular.forEach(data.products, function(productId) {
					singleton.getProduct(productId).then(function(item) {
						products.push(item);
					});
				});
				deferred.resolve(products);
				
			} else {
				deferred.reject(false);
			}
		});


		return deferred.promise;
	};

	this.getProduct = function(pid) {
		var deferred = $q.defer();

		this.getAllProducts(function(data) {
			var ret = false;
			angular.forEach(_products, function(p) {
				if (parseInt(p.id) == pid) {
					ret = p;
					return;
				}
			});
			(!ret) ? deferred.resolve("Product " + pid + " is not found") : deferred.resolve(ret);
		});

		return deferred.promise;
	};

}]);