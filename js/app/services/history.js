edApplication.provider("EDHistory", function() {
	
	this.history = [];

	this.$get = function() {
		return {
			"getLast" : function() {
				if (this.history.length > 0) {
					return this.history[this.history.length-1];
				} else {
					return false;
				}
			},
			"getHistory" : function() {
				return this.history;
			},
			"reset" : function() {
				this.history = [];
				return this;
			},
			"add" : function(element) {
				if (!angular.isDefined(this.history)) {
					this.history = [];
				}
				this.history.push(element);
				return this;
			},
			"update" : function (index, element) {
				if (index >= 0 && this.history[index]) {
					this.history[index] = element;
					return true;
				} else {
					return false;
				}
			},
			"removeAfter" : function(url) {
				var idxToRemove = -1;

				angular.forEach(this.history, function(obj, key) {
					if (angular.isDefined(obj.url) && obj.url == url) {
						idxToRemove = key;
					}
				});

				if (idxToRemove < 0) {
					return false;
				} else {
					this.history.splice(idxToRemove, this.history.length - idxToRemove);
					return true;
				} 
			},
			"getLength" : function() {
				return this.history.length;
			}
		};
	};
});