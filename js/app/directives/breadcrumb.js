edApplication.directive('edBreadcrumb',['EDHistory', function(EDHistory) {
	return {
		restrict: 'E',
		priority: 10,
		//terminal: false,
		templateUrl: 'breadcrumb.html',
		replace: true,
		transclude: false,
		scope: {},
		link: function(scope, $element, iattrs, ctrl) {
			scope.breadcrumb = EDHistory.getHistory();
		}
	};
}]);