'use strict';

var edApplication = angular.module("EDApp", ['ui.router', 'ui.select', 'ngLoader'])
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/')

  $stateProvider.state('main', {
    url: '/',
    templateUrl: 'main.html',
    controller: 'MainCtrl'
  })
  .state('question', {
    url: '/question/:qid',
    templateUrl: 'question.html',
    controller: 'QuestionCtrl'
  })
  .state('answer', {
      cache: false,
      url: '/answer/:qid/:aid',
      controller: 'AnswerCtrl'
  })
  .state('products', {
    cache: false,
    url: '/result/:aid/products',
    templateUrl: 'products.html',
    controller: 'ProductsCtrl'
  });
})